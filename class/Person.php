<?php

class Person {

    private $first_name;
    private $last_name;
    private $age;
    //private static $creation = 0;

    public function __construct($first_name, $last_name, $age) {
        $this->setFirst_name($first_name);
        $this->setLast_name($last_name);
        $this->setAge($age);
        //static::$creation = static::$creation + 1;
    }

    public function fullName() {
        return $this->getFirst_name() . " " . $this->getLast_name() . " " . $this->getAge();
    }

    // public static function showCreation() {
    //     echo static::$creation;
    // }

    // Setters
    private function setFirst_name($first_name) {
        $this->first_name = ucfirst($first_name);
    }
    private function setLast_name($last_name) {
        $this->last_name = strtoupper($last_name);
    }
    private function setAge($age) {
        $this->age = $age;
    }

    // Getters
    public function getFirst_name() {
        return ucfirst($this->first_name);
    }
    public function getLast_name() {
        return strtoupper($this->last_name);
    }
    public function getAge() {
        return $this->age;
    }

}

?>
<?php

    // require 'Person.php';

    class Animal extends Person {

        public function __construct($first_name, $last_name, $age) {
            parent::__construct($first_name, $last_name, $age);
        }

        public function maitre() {
            return parent::fullName();
        }

    }

?>
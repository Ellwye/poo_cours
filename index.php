<?php
    require 'class/autoload.php';

    $test = new Person('fanny', 'rouyer', 24);

    echo "Personne test : " . $test->fullName(); // affiche la personne via une method public
    echo "<br>";
   
    // echo "Nombre de personnes créées : ";
    // Person::showCreation(); // affiche le nombre de personnes créées.
    // echo "<br>"; 

    echo "Age : " . $test->getAge(); // Affiche l'age via le getter et setter
    echo "<br>";
    echo "<br>";

    $test2 = new Animal('Oria', 'Rouyer', 2);
    echo $test2->maitre();
    echo "<br>";
    echo $test2->getFirst_name() . " a " . $test2->getAge() . " ans.";
    echo "<br>";

?>